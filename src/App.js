import React, { Component, Suspense } from 'react';
import './App.css';
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import { Switch, Route, Redirect } from 'react-router-dom';

import SignUp from './components/SignUp/SignUp';
import Logout from './containers/Logout/Logout';
import Layout from './containers/Layout/Layout';
import Spinner from './components/UI/Spinner/Spinner';
import Categories from './containers/Categories/Categories';
// import Products from './containers/Products/Products';
// import Checkout from './containers/Checkout/Checkout';
// import CheckoutPayment from './containers/Checkout/CheckoutPayment';
// import CheckoutConfirm from './components/Checkout/CheckoutConfirm/CheckoutConfirm';
// import Orders from './containers/Orders/Orders';

const Products = React.lazy(() => {
    return import('./containers/Products/Products')
})

const Orders = React.lazy(() => {
    return import('./containers/Orders/Orders')
})

const Checkout = React.lazy(() => {
    return import('./containers/Checkout/Checkout')
})

const CheckoutPayment = React.lazy(() => {
    return import('./containers/Checkout/CheckoutPayment')
})

const CheckoutConfirm = React.lazy(() => {
    return import('./components/Checkout/CheckoutConfirm/CheckoutConfirm')
})

class App extends Component {
    componentDidMount() {
        this.props.onTryAutoLogin()
    }

    render() {
        let routes = (
            <Switch>
                <Route path='/products'
                    render={(props) => (
                        <Products {...props} />
                    )} />

                <Route path='/category/:name'
                    render={(props) => (
                        <Products {...props} />
                    )} />
                {/* <Route path='/category/:name' component={Products} /> */}
                <Route path="/signup" component={SignUp} />
                <Route path='/' exact component={Categories} />
                <Redirect to="/" />
            </Switch>
        )

        if (this.props.isAuthenticated) {
            routes = (
                <Switch>
                    <Route path='/orders'
                        render={(props) => (
                            <Orders {...props} />
                        )} />

                    <Route path='/checkout/details'
                        render={(props) => (
                            <Checkout {...props} />
                        )} />
                    <Route path='/checkout/payment'
                        render={(props) => (
                            <CheckoutPayment {...props} />
                        )} />
                    <Route path='/checkout/confirm'
                        render={(props) => (
                            <CheckoutConfirm {...props} />
                        )} />

                    <Route path='/products'
                        render={(props) => (
                            <Products {...props} />
                        )} />

                    <Route path='/category/:name'
                        render={(props) => (
                            <Products {...props} />
                        )} />
                    <Route path='/logout' component={Logout} />
                    <Route path='/' exact component={Categories} />
                    <Redirect to="/" />
                </Switch>
            )
        }

        return (
            <div className="App">
                <Layout>
                    <Suspense fallback={<Spinner />}>
                        {routes}
                    </Suspense>
                </Layout>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token != null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoLogin: () => dispatch(actions.authCheckState())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
