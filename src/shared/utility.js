export const checkValidation = (value, rules) => {

    let validation = {
        isValid: true,
        errorMessage: ''
    };

    if (!rules) {
        return validation;
    }

    if (rules.required) {
        // const valid = value.trim() !== '' && validation.isValid;
        const valid = value !== '' && validation.isValid;
        if (!valid) {
            validation = {
                isValid: false,
                errorMessage: 'This field is required'
            }
            return validation;
        }
    }

    if (rules.minLength) {
        const valid = value.length >= rules.minLength && validation.isValid;
        if (!valid) {
            validation = {
                isValid: false,
                errorMessage: `Minimum length is ${rules.minLength}`
            }
            return validation;
        }
    }

    if (rules.maxLength) {
        const valid = value.length <= rules.maxLength && validation.isValid;
        if (!valid) {
            validation = {
                isValid: false,
                errorMessage: `Maximum length is ${rules.minLength}`
            }
            return validation;
        }
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        const valid = pattern.test(value) && validation.isValid;

        if (!valid) {
            validation = {
                isValid: false,
                errorMessage: 'Email entered is not valid'
            }
            return validation;
        }
    }

    if (rules.isNumeric) {
        const pattern = /^\d+$/;
        const valid = pattern.test(value) && validation.isValid;

        if (!valid) {
            validation = {
                isValid: false,
                errorMessage: 'Number is required'
            }
            return validation;
        }
    }

    return validation;
}

export const capitalize = str => {
    return str.replace(/\w\S*/g, (txt) => { 
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); 
    });
}