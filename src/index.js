import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import reducers from './store/reducers/index';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import App from './App';

import './index.css';
import { loadState, saveState } from './localStorage';
import throttle from 'lodash/throttle';


const persistedState = loadState();
const composeEnhancers =  
    process.env.NODE_ENV === 'development' 
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ 
        : null || compose;

const store = createStore(
    reducers,
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(throttle(() => {
    saveState(store.getState())
}, 1000))


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter><App /></BrowserRouter>
    </Provider>, 
    document.getElementById('root')
);