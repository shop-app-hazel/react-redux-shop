import http from '../../services/http';
import * as actionTypes from './actionTypes';


// ------------- fetch products
export const fetchProductsStart = () => {
    return {
        type: actionTypes.FETCH_PRODUCTS_START
    }
}

export const fetchProductsSuccess = (data) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_SUCCESS,
        products: data.products,
        productCount: data.count
    }
}

export const fetchProductsFailed = (error) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_FAILED,
        error
    }
}

export const fetchProducts = (filterCategory) => async dispatch => {
    try {
        dispatch(fetchProductsStart());
        
        let response = await http.get(`/products?category=${filterCategory}`);
        // console.log('[store-action products.js] fetch products', response.data);
        
        dispatch(fetchProductsSuccess(response.data));
    } catch (err) {
        dispatch(fetchProductsFailed(err.response.data.error));
    }
}

export const searchProducts = (inputVal) => async dispatch => {
    try {
        dispatch(fetchProductsStart());
        let response = await http.get(`/products?search=${inputVal}`);
        dispatch(fetchProductsSuccess(response.data));
        
    } catch(err) {
        dispatch(fetchProductsFailed(err.response.data.error))
    }
}
