import * as actionTypes from './actionTypes';
import http from '../../services/http';

export const fetchCategoriesStart = () => {
    return { type: actionTypes.FETCH_CATEGORIES_START } 
}

export const fetchCategoriesSuccess = (categories) => {
    return {
        type: actionTypes.FETCH_CATEGORIES_SUCCESS,
        categories
    }
}

export const fetchCategoriesFailed = () => {
    return { type: actionTypes.FETCH_CATEGORIES_FAILED }
}

export const fetchCategories = () => async dispatch => {
    try {
        dispatch(fetchCategoriesStart());
        const response = await http.get('/categories');
        dispatch(fetchCategoriesSuccess(response.data.categories));
    } catch (e) {
        console.log(e);
        dispatch(fetchCategoriesFailed())
    }
};
   

export const selectCategory = category => {
    return {
        type: actionTypes.SELECT_CATEGORY,
        selectedCategory: category
    };
}