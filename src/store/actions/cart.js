import * as actionTypes from './actionTypes';
import http from '../../services/http';

export const getCartId = () => {
    const cartId = localStorage.getItem('shop-app-cartId');
    if (cartId) {
        return cartId;
    }
}

export const initCart = () => async dispatch => {
    try {
        const cartId = getCartId();
        if (cartId) {
            const response = await http.get(`/carts/${cartId}`);
            dispatch({
                type: actionTypes.INIT_CART,
                cart: response.data.cart
            });
        } else {
            dispatch({
                type: actionTypes.INIT_CART,
                cart: null
            });
        }

    } catch (e) {
        console.log(e);
    }
}

export const addToCartSuccess = (cart) => {
    return {
        type: actionTypes.ADD_TO_CART_SUCCESS,
        cart: cart
    }
}

export const addToCartFailed = (error) => {
    return {
        type: actionTypes.ADD_TO_CART_FAILED,
        error: error
    }
}

export const addProductToCart = (productId) => async dispatch => {
    try {
        let response = null;
        const cartId = getCartId();
        const data = { productId };
        if (cartId) {
            response = await http.patch(`/carts/${cartId}`, data);
        } else {
            response = await http.post('/carts', data);
            // console.log('[store-action cart.js] ', response)
            localStorage.setItem('shop-app-cartId', response.data.cart._id);
        }
        dispatch(addToCartSuccess(response.data.cart));
    } catch (e) {
        dispatch(addToCartFailed(e.response.data.error));
    }
}

export const updateCartSuccess = (cart) => {
    return {
        type: actionTypes.UPDATE_CART_SUCCESS,
        cart
    }
}

export const updateCartFailed = (error) => {
    return {
        type: actionTypes.UPDATE_CART_FAILED,
        error
    }
}

export const increaseProductQuantity = (productId) => async dispatch => {
    try {
        const cartId = getCartId();
        const data = {
            productId,
            quantity: null,
            isIncrease: true
        }
        const response = await http.patch(`/carts/${cartId}`, data);
        dispatch(updateCartSuccess(response.data.cart));
        
    } catch (e) {
        // console.log('failed to increase ', e)
        dispatch(updateCartFailed(e.response.data.error))
    }
}


export const decreaseProductQuantity = (productId) => async dispatch => {
    try {
        const cartId = getCartId();
        const data = {
            productId,
            quantity: null,
            isIncrease: false
        }
        const response = await http.patch(`/carts/${cartId}`, data);
        dispatch(updateCartSuccess(response.data.cart));

    } catch (e) {
        // console.log('failed to decrease ', e)
        dispatch(updateCartFailed(e.response.data.error))
    }
}

export const cancelProduct = (productId) => async dispatch => {
    try {
        const cartId = getCartId();
        const data = {
            productId,
            quantity: 0,
            isIncrease: false
        }
        const response = await http.patch(`/carts/${cartId}`, data);
        dispatch(updateCartSuccess(response.data.cart));

    } catch (e) {
        dispatch(updateCartFailed(e.response.data.error))
    }
}

export const updateInputQuantity = (value, productId) => async dispatch => {
    try {
        const cartId = getCartId();
        const data = {
            productId,
            value: value
        }
        const response = await http.patch(`/carts/${cartId}`, data);
        console.log(response)
    } catch (e) {
        console.log('failed to decrease ', e)
    }
}