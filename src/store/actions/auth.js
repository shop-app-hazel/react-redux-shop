import * as actionTypes from './actionTypes'
import http from '../../services/http'
import jwt_decode from 'jwt-decode'

const decodeToken = (token) => {
    const decodedToken = jwt_decode(token)
    // console.log(decodedToken)
    return decodedToken
}


export const authCheckState = () => {
    return dispatch => {
        
        const token = localStorage.getItem('shop-app-token');
        if(!token) {
            dispatch(logout());
        } else {
            const decodedToken = decodeToken(token)
            const expDate = new Date(decodedToken.exp * 1000);
            // console.log('token expiry ', expDate)
         
            if(expDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = decodedToken.userId;
                dispatch(authSuccess(token, userId));

                // console.log('check auth timeout', (expDate.getTime() - new Date().getTime())/1000 )
                dispatch(checkAuthTimeout((expDate.getTime() - new Date().getTime())/1000) );
            }
        }
    }
}

export const authStart = () => {
    return { type: actionTypes.AUTH_START }
}

export const authSuccess = (token, userId) => {
    return { 
        type: actionTypes.AUTH_SUCCESS,
        token,
        userId
    }
}

export const authFailed = (error) => {
    return { 
        type: actionTypes.AUTH_FAILED,
        error
     }
}

export const checkAuthTimeout = (expTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expTime * 1000);
    }
}

export const auth = (authData) => async dispatch => {
    try {
        dispatch(authStart())

        const response = await http.post('/users/login', authData)
        // console.log('[action auth.js response]' , response);

        const decodedToken = decodeToken(response.data.token)

        localStorage.setItem('shop-app-token', response.data.token);
        localStorage.setItem('shop-app-userId', decodedToken.userId);

        dispatch(authSuccess(response.data.token, response.data.user._id))

        const expDate = new Date(decodedToken.exp * 1000);
        // console.log('check auth timeout', (expDate.getTime() - new Date().getTime())/1000 )
        dispatch(checkAuthTimeout((expDate.getTime() - new Date().getTime())/1000));
        
    } catch(err) {
        // console.log('[action auth.js] ', err.response.data.error)
        dispatch(authFailed(err.response.data.error))
    }
}


export const signUp = (signupData) => async dispatch => {
    try {
        dispatch(signUpStart())
        let formData = {
            email: signupData.email,
            firstName: signupData.firstName,
            lastName: signupData.lastName,
            password: signupData.password,
            phone: signupData.phone,
            address: {
                street: signupData.street,
                suburb: signupData.suburb,
                city: signupData.city,
                postal: signupData.postal
            }
        }
        await http.post('/users/signup', formData)
        dispatch(signUpSuccess())
    } catch(err) {
        dispatch(signUpFailed(err.response.data.error))
    } 
}

export const signUpStart = () => {
    return { type: actionTypes.SIGNUP_START }
}

export const signUpSuccess = () => {
    return { type: actionTypes.SIGNUP_SUCCESS }
}

export const signUpFailed = (error) => {
    return { type: actionTypes.SIGNUP_FAILED, error}
}

export const logout = () => {
    localStorage.removeItem('shop-app-token');
    localStorage.removeItem('shop-app-userId');
    localStorage.removeItem('state')
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}