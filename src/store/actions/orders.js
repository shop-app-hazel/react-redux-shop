import http from '../../services/http'
import * as actionTypes from './actionTypes'

export const fetchOrderByLoginUserStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_BY_LOGIN_USER_START,
        isLoading: true
    }
}

export const fetchOrderByLoginUserSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_BY_LOGIN_USER_SUCCESS,
        isLoading: false,
        error: null,
        orders: orders
    }
}

export const fetchOrderByLoginUserFailed = error => {
    return {
        type: actionTypes.FETCH_ORDERS_BY_LOGIN_USER_FAILED,
        isLoading: false,
        error: error
    }
}

export const fetchOrdersByLoginUser = () => async dispatch => {
    try {
        dispatch(fetchOrderByLoginUserStart())

        const response = await http.get('/orders')
        // console.log('[order.js actions] fetch orders by login user ', response)
        dispatch(fetchOrderByLoginUserSuccess(response.data.orders))
    } catch (err) {
        dispatch(fetchOrderByLoginUserFailed(err.response.data.error))
    }
}