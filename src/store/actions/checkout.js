import http from '../../services/http'
import * as actionTypes from './actionTypes'

export const updatePaymentForm = (formData, isValid) => {
    return {
        type: actionTypes.UPDATE_PAYMENT_FORM,
        formData,
        isValid
    }
}

export const checkoutOrderStart = () => {
    return {
        type: actionTypes.CHECKOUT_ORDER_START
    }
}

export const checkoutOrderSuccess = (orders) => {
    return {
        type: actionTypes.CHECKOUT_ORDER_SUCCESS,
        orders: orders
    }
}

export const checkoutOrderFailed = (error) => {
    return {
        type: actionTypes.CHECKOUT_ORDER_FAILED,
        error: error
    }
}

export const checkoutOrder = (paymentForm) => async dispatch => {
    try {
        dispatch(checkoutOrderStart())
        let formData = {
            cartId: localStorage.getItem('shop-app-cartId'),
            payment: paymentForm,
        }
        // console.log('[order.js] orderForm ', formData)

        await http.post('/orders', formData)
        // console.log('[order.js] response ', response)
        dispatch(checkoutOrderSuccess())
    } catch (err) {
        dispatch(checkoutOrderFailed(err.response.data.error))
    }
}

export const checkoutCompleted = () => {
    localStorage.removeItem('shop-app-cartId')
    return {
        type: actionTypes.CHECKOUT_COMPLETED
    }
}
