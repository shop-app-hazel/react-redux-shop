import * as actionTypes from '../actions/actionTypes'

const initialState = {
    isLoading: false,
    error: null,
    orders: []
}

const orders = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_ORDERS_BY_LOGIN_USER_START:
            return {
                ...state,
                isLoading: true,
                error: null
            }

        case actionTypes.FETCH_ORDERS_BY_LOGIN_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                error: null,
                orders: action.orders
            }
        case actionTypes.FETCH_ORDERS_BY_LOGIN_USER_FAILED:
            return {
                ...state,
                isLoading: false,
                error: action.error
            }
        default: return state;
    }
}

export default orders;