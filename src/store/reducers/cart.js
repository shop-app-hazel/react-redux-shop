import * as actionTypes from '../actions/actionTypes';


const initialState = {
    cart: null,
    error: null
}

const initCart = (state, action) => {
    return {
        ...state, 
        cart: action.cart
    }
}

const addToCartSuccess = (state, action) => {
    return {
        ...state,
        cart: action.cart
    }
}

const addToCartFailed = (state, action) => {
    return {
        ...state,
        error: action.error
    }
}

const updateCartSuccess = (state, action) => {
    return {
        ...state,
        cart: action.cart
    }
}

const updateCartFailed = (state, action) => {
    return {
        ...state,
        error: action.error
    }
}

const checkoutCompleted = (state, action) => {
    return {
        ...state,
        cart: null
    }
}


const cart = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.INIT_CART: return initCart(state, action);
        case actionTypes.ADD_TO_CART_SUCCESS: return addToCartSuccess(state, action);
        case actionTypes.ADD_TO_CART_FAILED: return addToCartFailed(state, action);
      
        case actionTypes.UPDATE_CART_SUCCESS: return updateCartSuccess(state, action);
        case actionTypes.UPDATE_CART_FAILED: return updateCartFailed(state,action);

        case actionTypes.CHECKOUT_COMPLETED: return checkoutCompleted(state, action);
        default: return state;
    }
}

export default cart;

// ***** UPDATE QUANTITY IN BROWSER (not using as changed to update in server)****//
// const updateQuantity = (state, action) => {
//     let item = state.cart.products.find(item => item.id === action.productId);
   
//     return {
//         ...state,
//         cart: {
//             ...state.cart,
//             totalPrice: (parseFloat(state.cart.totalPrice) + parseFloat(item.price)).toFixed(2),
//             products: state.cart.products.map((product, i) => 
//                                         product.id === item.id
//                                         ? {...product, count: action.isIncrease ? item.count += 1 : item.count -= 1}
//                                         : product
//             )
//         }
//     }
// }