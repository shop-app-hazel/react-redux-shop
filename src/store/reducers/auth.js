import * as actionTypes from '../actions/actionTypes'

const initialState = {
    userId: null,
    token: null,
    isLoading: false,
    error: null
}

const authStart = (state, action) => {
    return {
        ...state,
        isLoading: true
    }
}

const authSuccess = (state, action) => {
    return {
        ...state,
        error: null,
        isLoading: false,
        userId: action.userId,
        token: action.token
    }
}

const authFailed = (state, action) => {
    return {
        ...state, 
        isLoading: false,
        error: action.error
    }
}

const signupStart = (state, action) => {
    return {
        ...state,
        isLoading: true,
        error: null
    }
}

const signupSuccess = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: null
    }
}

const signUpFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const logout = (state, action) => {
    return {
        ...state,
        userId: null,
        token: null
    }
}

const auth = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAILED: return authFailed(state, action);
        case actionTypes.SIGNUP_START: return signupStart(state, action);
        case actionTypes.SIGNUP_SUCCESS: return signupSuccess(state, action);
        case actionTypes.SIGNUP_FAILED: return signUpFailed(state, action);
        case actionTypes.AUTH_LOGOUT: return logout(state, action);
        default: return state
    }
}

export default auth;