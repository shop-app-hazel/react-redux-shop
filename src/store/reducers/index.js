import { combineReducers } from 'redux';
import categories from './categories';
import products from './products';
import cart from './cart';
import auth from './auth';
import checkout from './checkout';
import orders from './orders';

export default combineReducers ({
    categories,
    products,
    cart,
    auth,
    checkout,
    orders
});
