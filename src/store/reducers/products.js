import * as actionTypes from '../actions/actionTypes';

const initialState = {
    products: [],
    isLoading: false,
    error: null,
    product: {},
    productCount: 0
}

const fetchProductsStart = (state, action) => {
    return {
        ...state,
        isLoading: true
    }
}

const fetchProductsSuccess = (state, action) => {
    return {
        ...state,
        isLoading: false,
        products: action.products,
        productCount: action.productCount
    }
}

const fetchProductsFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const products = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.FETCH_PRODUCTS_START: return fetchProductsStart(state, action);
        case actionTypes.FETCH_PRODUCTS_SUCCESS: return fetchProductsSuccess(state, action);
        case actionTypes.FETCH_PRODUCTS_FAILED: return fetchProductsFailed(state, action);
        default: return state;
    }
}

export default products;