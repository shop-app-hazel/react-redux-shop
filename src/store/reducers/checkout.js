import * as actionTypes from '../actions/actionTypes'

const initialState = {
    paymentForm: null,
    isPaymentFormValid: false,
    error: null,
    isLoading: false
}

const checkout = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_PAYMENT_FORM:
            return {
                ...state,
                paymentForm: action.formData,
                isPaymentFormValid: action.isValid
            }
        case actionTypes.CHECKOUT_ORDER_START:
            return {
                ...state,
                isLoading: true,
                error: null,
                isPaymentFormValid: false
            }
        case actionTypes.CHECKOUT_ORDER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                error: null
            }
        case actionTypes.CHECKOUT_ORDER_FAILED:
            return {
                ...state,
                isLoading: false,
                error: action.error
            }
        default: return state;
    }
}

export default checkout;