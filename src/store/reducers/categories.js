import * as actionTypes from '../actions/actionTypes';

const initialState = {
    categories: [],
    loading: false,
    selectedCategory: null
}

const fetchCategoriesStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const fetchCategoriesSuccess = (state, action) => {
    return {
        ...state,
        categories: action.categories,
        loading: false
    };
}

const fetchCategoriesFailed = (state, action) => {
    return {
        ...state,
        loading: false
    }
}

const selectCategory = (state, action) => {
    return {
        ...state,
        selectedCategory: action.selectedCategory
    };
}

const categories = (state = initialState, action) => {
    switch (action.type) {    
        case actionTypes.FETCH_CATEGORIES_START: return fetchCategoriesStart(state, action);
        case actionTypes.FETCH_CATEGORIES_SUCCESS: return fetchCategoriesSuccess(state, action);
        case actionTypes.FETCH_CATEGORIES_FAILED: return fetchCategoriesFailed(state, action);
        
        case actionTypes.SELECT_CATEGORY: return selectCategory(state, action);
        default: return state;   
    }
} 

export default categories;