import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import '../../App.css';
import { connect } from 'react-redux'
import * as actions from '../../store/actions/index'
import Input from '../UI/Input/Input'
import { checkValidation } from '../../shared/utility'
import MaterialButton from '../UI/MaterialButton/MaterialButton';
import MaterialSnackbar from '../UI/MaterialSnackbar/MaterialSnackbar';
import { Grid } from '@material-ui/core';


class SignUp extends Component {
    state = {
        isSnackbarOpen: false,
        successSignUp: false,
        error: null,
        isFormValid: false,
        signupForm: {
            email: {
                label: 'Email Address',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true,
                    isEmail: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            firstName: {
                label: 'First Name',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            lastName: {
                label: 'Last Name',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            password: {
                label: 'Password',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'password',
                    placeholder: ''
                },
                validationRules: {
                    required: true,
                    minLength: 6
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            phone: {
                label: 'Phone',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            street: {
                label: 'Street',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            suburb: {
                label: 'Suburb',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            city: {
                label: 'City',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            postal: {
                label: 'Postal',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
        }
    }

    inputChangedHandler = (e, formId) => {
        const updatedForm = {
            ...this.state.signupForm,
            [formId]: {
                ...this.state.signupForm[formId],
                value: e.target.value,
                validation: checkValidation(e.target.value, this.state.signupForm[formId].validationRules),
                touched: true
            }
        }
        let isFormValid = true;
        for (let element in updatedForm) {
            isFormValid = updatedForm[element].validation.isValid && isFormValid;
        }
        this.setState({ signupForm: updatedForm, isFormValid })
    }

    signUpHandler = async (e) => {
        e.preventDefault();

        let formData = {}
        for (let elKey in this.state.signupForm) {
            formData[elKey] = this.state.signupForm[elKey].value;
        }

        this.setState({ isSnackbarOpen: true })
        await this.props.onSignup(formData)
        if (!this.props.error) {
            this.setState({ error: null })
            this.setState({ successSignUp: true })
        } else {
            this.setState({ error: this.props.error })
        }
    }

    closeSnackbarHandler = () => {
        this.setState({ isSnackbarOpen: false })
    }


    render() {
        const formElementsArray = [];
        for (let key in this.state.signupForm) {
            formElementsArray.push({
                id: key,
                config: this.state.signupForm[key]
            });
        }

        let renderErrorSnackbar = null
        let renderSignUpContent = null

        if (this.state.error) {
            renderErrorSnackbar = <MaterialSnackbar
                vertical="bottom"
                horizontal="left"
                variant="error"
                snackbarState={this.state.isSnackbarOpen}
                onSnackbarClose={this.closeSnackbarHandler}
                message={this.state.error} />
        }

        if (this.state.successSignUp) {
            renderSignUpContent =
                <div>
                    <span className="HeaderTitle">Welcome to I Shop App</span>
                    <p>Your account has been created succesfully</p>
                    <Link to="/">
                        <MaterialButton
                            isFullWidth={false}
                            btnVariant="contained"
                            btnColor="primary"
                            btnName="Continue shopping"
                            isDisabled={false} />
                    </Link>
                </div>
        } else {
            renderSignUpContent = (
                <React.Fragment>
                    <span className="HeaderTitle">
                        Register a New Account
                        </span>

                    <Grid container spacing={2}>
                        <Grid item md={3}></Grid>
                        <Grid item md={6} xs={12}>
                            <form>
                                {formElementsArray.map((formElement, index) => (
                                    <Input
                                        id={formElement.id}
                                        key={formElement.id}
                                        label={formElement.config.label}
                                        value={formElement.config.value}
                                        helperText={formElement.config.helperText}
                                        elementType={formElement.config.elementType}
                                        elementConfig={formElement.config.elementConfig}
                                        isValid={formElement.config.validation.isValid}
                                        isTouched={formElement.config.touched}
                                        errorMessage={formElement.config.validation.errorMessage}
                                        textFieldVariant="outlined"
                                        onInputChanged={(e) => this.inputChangedHandler(e, formElement.id)}
                                    />
                                ))}
                                <div style={{ margin: '20px 0' }}>
                                    <MaterialButton
                                        isFullWidth={true}
                                        btnVariant="contained"
                                        btnColor="primary"
                                        btnName="Create Account"
                                        isDisabled={!this.state.isFormValid}
                                        onButtonClicked={this.signUpHandler} />
                                </div>
                            </form>
                        </Grid>
                    </Grid>
                </React.Fragment>
            )
        }


        return (
            <Fragment>
                {renderErrorSnackbar}

                <div className="Container">
                    <div style={{ textAlign: 'center' }}>
                        {renderSignUpContent}
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        error: state.auth.error,
        signupSuccess: state.auth.signupSuccess
    }
}

const maptDispatchToProps = dispatch => {
    return {
        onSignup: (formData) => dispatch(actions.signUp(formData))
    }
}

export default connect(mapStateToProps, maptDispatchToProps)(SignUp);