import React from 'react';
import styles from './Category.module.css';

import {
    Grid, 
    Card,
    CardMedia,
    CardActionArea
} from '@material-ui/core';


const Category = (props) => {

    return(
        <React.Fragment>
            <Grid item md={4} sm={6} xs={12}>
                <Card>
                    <CardActionArea onClick={() => {props.onSelectCategory(props.category)}}>
                        <CardMedia component="img"
                                    height="140" width="200"
                                    src={props.category.imgPath}   />
                        <div className={styles.ImgOverlay}>   
                            {props.category.name}
                        </div>
                    </CardActionArea>  
                </Card>
            </Grid>
        </React.Fragment>
    )
}

export default Category;