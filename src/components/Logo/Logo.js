import React from 'react';
import { withRouter } from 'react-router-dom';
import classes from './Logo.module.css';
import logo from '../../assets/images/shop-client.jpg';

const Logo = (props) => {
    const imageClickHandler = () => {
        props.history.push('/');
    }
  
    return( 
        <div className={classes.Logo} onClick={imageClickHandler}>
            <img src={logo} alt="shop" />
        </div>
    )
}

export default withRouter(Logo);