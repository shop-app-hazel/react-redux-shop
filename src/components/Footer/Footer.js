import React from 'react'
import classes from './Footer.module.css';

const Footer = () => {
    return (
        <div className={classes.Footer}>
            hazel &copy; 2019.
        </div>
    )
}

export default Footer;