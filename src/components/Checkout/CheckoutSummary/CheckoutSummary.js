import React from 'react'
import { connect } from 'react-redux'
import * as actions from '../../../store/actions/index'
import { withRouter } from 'react-router-dom'
import classes from './CheckoutSummary.module.css'
import { Card, CardContent, Divider } from '@material-ui/core';
import MaterialButton from '../../UI/MaterialButton/MaterialButton';


const CheckoutSummary = props => {

    const checkoutHandler = async () => {
        if(props.history.location.pathname === '/checkout/details') {
            props.history.push('/checkout/payment')

        } else if (props.history.location.pathname === '/checkout/payment') {
            try {
                await props.onCheckoutOrder(props.paymentFormData);
    
                if(!props.error) {
                    props.history.push('/checkout/confirm')
                    props.onCheckoutCompleted()

                } else {
                    alert('[CheckoutSummary.js]', props.error)
                }
            } catch (err) {
                alert('[CheckoutSummary.js]', err)
            }
        }
    }
    
    let btnName = "checkout"
    if(props.history.location.pathname === '/checkout/details') {
        btnName = "checkout"
    } else if (props.history.location.pathname === '/checkout/payment') {
        btnName = "place order"
    }

    return (
        <React.Fragment>
            <Card>
                <CardContent>
                    <div className={classes.SummaryFee}>
                        <span className={classes.SummaryHeader}>Order Summary</span>
                        <span>{props.cart ? props.cart.totalQuantity : null} items</span>
                    </div>

                    <Divider className={classes.SummaryDivider} />

                    <div className={classes.SummaryFee} style={{ fontWeight: 'bolder' }}>
                        <span>Groceries</span>
                        <span>${props.cart ? props.cart.totalPrice.toFixed(2) : null}</span>
                    </div>
                 
                    <Divider className={classes.SummaryDivider} />

                    <div className={classes.SummaryFee}
                        style={{ fontWeight: 'bolder', fontSize: '20px', marginBottom: '0' }}>
                        <span>Estimated Total</span>
                        <span>
                            ${props.cart ? props.cart.totalPrice.toFixed(2) : null}
                        </span>
                    </div>
                    <span>incl. GST</span>

                    <Divider className={classes.SummaryDivider} />

                    <div className={classes.CheckoutButton}>
                        <MaterialButton
                            btnName={btnName}
                            btnColor="primary"
                            btnVariant="contained"
                            isDisabled={!props.isValid}
                            onButtonClicked={checkoutHandler} />
                    </div>

                </CardContent>
            </Card>
        </React.Fragment>
    )
}

const mapStateToProps = state => {
    return {
       paymentFormData: state.checkout.paymentForm,
       error: state.checkout.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCheckoutOrder: (paymentForm) => dispatch(actions.checkoutOrder(paymentForm)),
        onCheckoutCompleted: () => dispatch(actions.checkoutCompleted())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CheckoutSummary));