import React from 'react'
import classes from './CheckoutConfirm.module.css'
import MaterialButton from '../../UI/MaterialButton/MaterialButton';

const CheckoutConfirm = props => {
    // console.log('[CheckoutConfirm.js] ', props)

    const continueHandler = () => {
        props.history.push('/')
    }
    return (
        <div className={classes.Container}>
            <h3>Thank you for shopping with us!</h3>
            <p>Your order has been placed.
                Please check your email for invoice.
            </p>

            <div className={classes.ContinueButton}>
                <MaterialButton
                    btnVariant="contained"
                    btnColor="primary"
                    btnName="continue shopping"
                    onButtonClicked={continueHandler} />
            </div>
        </div>
    )
}

export default CheckoutConfirm