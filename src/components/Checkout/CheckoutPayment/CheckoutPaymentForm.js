import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../../store/actions/index'
import { checkValidation } from '../../../shared/utility';
import Input from '../../UI/Input/Input';

class CheckoutPaymentForm extends Component {
    state = {
        error: null,
        // isFormValid: false,
        paymentForm: {
            ccNumber: {
                label: 'Card Number',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            ccExpiry: {
                label: 'Card Expiry',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
            ccName: {
                label: 'Card Holder Name',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            }
        }
    }

    inputChangedHandler = (e, formId) => {
        const updatedForm = {
            ...this.state.paymentForm,
            [formId]: {
                ...this.state.paymentForm[formId],
                value: e.target.value,
                validation: checkValidation(e.target.value, this.state.paymentForm[formId].validationRules),
                touched: true
            }
        }
        
        let isFormValid = true;
        for (let element in updatedForm) {
            isFormValid = updatedForm[element].validation.isValid && isFormValid;
        }
        this.setState({ paymentForm: updatedForm })

        const formData = {};
        for (let formElement in updatedForm) {
            formData[formElement] = updatedForm[formElement].value;
        }
        this.props.onPaymentFormUpdate(formData, isFormValid)  
    }


    render() {
        const formElementsArray = [];
        for (let key in this.state.paymentForm) {
            formElementsArray.push({
                id: key,
                config: this.state.paymentForm[key]
            });
        }
    
        return (
            <form>
                {formElementsArray.map((formElement, index) => (
                    <div style={{ marginBottom: '20px' }} key={formElement.id}>
                        <Input
                            id={formElement.id}
                            label={formElement.config.label}
                            value={formElement.config.value}
                            helperText={formElement.config.helperText}
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            isValid={formElement.config.validation.isValid}
                            isTouched={formElement.config.touched}
                            errorMessage={formElement.config.validation.errorMessage}
                            textFieldVariant="outlined"
                            onInputChanged={(e) => this.inputChangedHandler(e, formElement.id)}
                        />
                    </div>
                ))}
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPaymentFormUpdate: (formData, isValid) => dispatch(actions.updatePaymentForm(formData, isValid))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPaymentForm);