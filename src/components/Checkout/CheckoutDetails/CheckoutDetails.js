import React from 'react'
import classes from './CheckoutDetails.module.css';
import ProductControl from '../../Products/ProductControl/ProductControl';
import { Card, CardContent, Grid } from '@material-ui/core';
import { CloseOutlined } from '@material-ui/icons';

const CheckoutDetails = props => {

    return (
        <React.Fragment>
            {props.cart.products.map(p => {
                return (
                    <Card key={p.product._id} className={classes.ProductCard}>
                        <div className={classes.RemoveIcon}>
                            <CloseOutlined />
                        </div>

                        <CardContent>
                            <Grid container spacing={3}>
                                <Grid item lg={2} sm={2} xs={6} className={classes.CardContent}>
                                    <div className={classes.ProductImage}>
                                        <img alt='p.product' src={p.product.image} width='100%' />
                                    </div>
                                </Grid>

                                <Grid item lg={2} sm={3} xs={6} className={classes.CardContent}>
                                    <span>{p.product.title}</span>
                                </Grid>

                                <Grid item lg={4} sm={4} xs={8} className={classes.CardContent}>
                                    <ProductControl product={p.product} isShowCartDrawer={true} />
                                </Grid>

                                <Grid item lg={3} sm={3} xs={3} className={classes.CardContent}>
                                    <span className={classes.Price}>
                                        ${(p.product.price * p.quantity).toFixed(2)}
                                    </span>
                                </Grid>

                                <Grid item lg={1} className={classes.CardContentRemoveIcon}>
                                    <CloseOutlined onClick={() => props.onCancelProduct(p.product)}/>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                )
            })
            }
        </React.Fragment>
    )
}

export default CheckoutDetails;