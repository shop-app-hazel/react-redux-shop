import React from 'react'
import classes from './OrderCard.module.css'
import { Grid } from '@material-ui/core'
import dateFormat from 'dateformat'

const OrderCard = props => {
    const { order } = props

    const convertDate = (oriDate) => {
        const newDate = new Date(oriDate).toLocaleString()
        // console.log(dateFormat(newDate, "dddd, mmmm dS, yyyy, h:MM:ss TT"))
        return dateFormat(newDate, "dddd, mmmm dS, yyyy, h:MM:ss TT")
    }

    return (
        <div className={classes.OrderCard}>
            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <strong>ORDER NO</strong>
                </Grid>

                <Grid item xs={6}>
                    {order.orderNum}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    Order placed on
                </Grid>

                <Grid item xs={6}>
                    {convertDate(order.orderDate)}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    Items Ordered
                </Grid>

                <Grid item xs={6}>
                    {order.totalQuantity}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    Total
                </Grid>

                <Grid item xs={6}>
                    <strong>${order.totalPrice.toFixed(2)}</strong>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <div className={classes.ProductsWrapper}>
                    {order.products.map(p => (
                        <div key={p.product.title} className={classes.ProductImgContainer}>
                            <img src={p.product.image} alt={p.product.title} height="80%" />
                            <span>
                                x {p.product.unit === 'ea' ? p.quantity : `${p.quantity.toFixed(1)} kg`}
                            </span>
                        </div>
                    ))}
                </div>
            </Grid>

        </div>
    )
}

export default OrderCard;