import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index'
import Spinner from '../UI/Spinner/Spinner';
import OrderCard from '../../components/Orders/OrderCard/OrderCard'


class OrderList extends Component {
    componentDidMount() {
        this.props.onFetchOrders()
    }

    render() {
        let renderOrders = <Spinner />
        if (this.props.orders.length <= 0) {
            renderOrders = <p>You do not have any orders.</p>
        } else {
            renderOrders = (
                this.props.orders.map(order => (
                    <OrderCard key={order._id} order={order} />
                ))
            )
        }

        return (
            <Fragment>
                {renderOrders}
            </Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        orders: state.orders.orders
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchOrders: () => dispatch(actions.fetchOrdersByLoginUser())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(OrderList)