import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classes from './CartDrawerContent.module.css'
import { Divider, Dialog } from '@material-ui/core';
import { KeyboardArrowLeftOutlined } from '@material-ui/icons'

import ProductControl from '../Products/ProductControl/ProductControl';
import MaterialButton from '../UI/MaterialButton/MaterialButton';
import Login from '../../containers/Auth/Login';

class CartDrawerContent extends Component {
    state = {
        isDialogOpen: false
    }


    closeDialogHandler = (e) => {
        this.setState({ isDialogOpen: false })
        this.props.onCloseDrawer(e);
    }

    checkoutHandler = (e) => {
        if(this.props.isLogin) {
            this.props.onCloseDrawer(e);
            this.props.history.push('/checkout/details');
        } else {
            this.setState({ isDialogOpen: true })
        }
    }

    render() {
        let renderCartDetails = null;
        if(this.props.cart) {
            renderCartDetails = (
                this.props.cart.products.map(p => {
                    return (
                        <React.Fragment key={p.product._id}>
                            <div className={classes.ProductWrapper}>
                                <div className={classes.ProductImg}>
                                    <img alt={p.product.title} src={p.product.image} width='100%' />
                                </div>
                                <div className={classes.ProductInfo}>
                                    <div className={classes.ProductDetail} >
                                        <div className={classes.ProductTitle}>{p.product.title}</div>
                                        <span style={{fontSize: '18px'}}>
                                            ${(p.product.price * p.quantity).toFixed(2)}
                                        </span>
                                    </div>
    
                                    <ProductControl product={p.product} isShowCartDrawer={true}/>
                                </div>
                            </div>
                            <Divider />
                        </React.Fragment>
                    )
                })
            )
        }
    
        return(
            <React.Fragment>
                <div className={classes.Content} >
                    <div className={classes.ContentWrapper}>
                        <div className={classes.TotalItemWrapper}>
                            <KeyboardArrowLeftOutlined className={classes.ArrowLeft}
                                onClick={this.props.onCloseDrawer}/>
                            <span>Your Trolley ({this.props.cart.totalQuantity})</span>
                        </div>
                        <Divider />
                        <div>
                            <div className={classes.Amount} >
                                <span>Groceries</span>
                                <span style={{fontSize: '18px'}}>${this.props.cart.totalPrice.toFixed(2)}</span>
                            </div>
    
                            <div className={classes.CheckoutBtn}>
                                <MaterialButton    
                                    btnVariant="contained"
                                    btnColor="primary"
                                    btnName="proceed to checkout"
                                    onButtonClicked={e => this.checkoutHandler(e)} />
                            </div>
                        </div>
                        <Divider />
                    </div>
    
                    <div className={classes.ProductBody}>
                        { renderCartDetails }
                    </div>
                </div>

                <Dialog open={this.state.isDialogOpen} onClose={this.closeDialogHandler}>
                    <Login onAction={e => this.closeDialogHandler(e)} />
                </Dialog>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart.cart,
        isLogin: state.auth.token !== null
    }
}

CartDrawerContent.propTypes = {
    onCloseDrawer: PropTypes.func
}

export default connect(mapStateToProps, null)(withRouter(CartDrawerContent));