import React from 'react';
import ProductControl from '../ProductControl/ProductControl';
import classes from './ProductCard.module.css';
import {
    Typography,
    Card,
    CardMedia,
    CardContent,
    CardActions
} from '@material-ui/core';


const ProductCard = (props) => {

    return (
        <React.Fragment>
            <div className={classes.ProductContainer} >
                <Card style={{borderRadius: '0'}}>
                    <CardContent>
                        <Typography className={classes.ProductTitle}>
                            {props.product.title}
                        </Typography>
                    </CardContent>
                    <CardMedia
                        component="img" className={classes.ProductImage}
                        height="150" width="20"
                        src={props.product.image} />

                    <CardContent className={classes.ProductPrice}>
                        <Typography component="h6" variant="h6">
                            {props.product.price.toFixed(2)}
                            <span style={{ fontSize: '12px' }}>{props.product.unit}</span>
                        </Typography>
                    </CardContent>

                    <CardActions style={{padding: '8px 0'}}>
                        <ProductControl product={props.product} isShowCartDrawer={false} />
                    </CardActions>
                </Card>
            </div>
        </React.Fragment>
    )
}


export default ProductCard;