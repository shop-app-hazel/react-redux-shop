import React, { Fragment, Component } from 'react'
import { connect } from 'react-redux';
import './ProductList.css'
import * as actions from '../../store/actions/index';
import ProductCard from './ProductCard/ProductCard';
import Spinner from '../UI/Spinner/Spinner';


class ProductList extends Component {
    componentDidMount() {
        this.props.onFetchProducts(this.props.filter);
    }

    render() {
        // console.log('[ProductList.js] ', this.props)
        let renderProducts = <Spinner />;

        if (!this.props.isLoading) {
            if (this.props.products.length === 0) {
                renderProducts = (
                    <React.Fragment>
                        <h2>Product not found</h2>
                        <p>Sorry, we can't find that item. Please try another search.</p>
                    </React.Fragment>
                )
            } else {
                renderProducts = (
                    <div className="ProductsContainer">
                        {
                            this.props.products.map(product => (
                                <ProductCard
                                    key={product._id}
                                    cart={this.props.cart}
                                    product={product} />
                            ))
                        }
                    </div >
                )
            }
        }

        return (
            <Fragment>
                {renderProducts}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.products.isLoading,
        products: state.products.products,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: (filterCategory) => dispatch(actions.fetchProducts(filterCategory))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductList);