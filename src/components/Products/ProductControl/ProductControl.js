import React from 'react';
import { connect } from 'react-redux';
import '../../../App.css';
import classes from './ProductControl.module.css';
import PropTypes from 'prop-types';
import * as actions from '../../../store/actions/index';
import { Button } from '@material-ui/core';
import { Add, Remove } from '@material-ui/icons';

class ProductControl extends React.Component {


    // inputHandler = (e, productId, currentQuantity) => {
    //     //TODO: check if input is number 
    //     const updatedQuantity = e.target.value;
    //     console.log(updatedQuantity)

    //     if (currentQuantity > +updatedQuantity) {
    //         this.setState({
    //             ...this.state,
    //             quantity: e.target.value,
    //             isIncrease: false,
    //             productId: productId
    //         });
    //     } else {
    //         this.setState({
    //             ...this.state,
    //             quantity: e.target.value,
    //             isIncrease: true,
    //             productId: productId
    //         });
    //     }
    // }

    shouldComponentUpdate(prevProps) {
        if (prevProps.cart !== this.props.cart) {
            return true
        }
    }

    render() {
        let renderControls =
                <Button fullWidth variant="contained" color="primary"
                    onClick={() => this.props.onAddToCart(this.props.product._id)}>
                    Add
                </Button>

        let renderProductCount =
                <input
                    type='text'
                    className={classes.Input}
                    readOnly
                    value={this.props.product.unit === 'ea' ? 1 : 0.1} />

        if (this.props.cart) {
            const filteredProduct = this.props.cart.products.find(
                cartProduct => cartProduct.product._id === this.props.product._id);

            if (filteredProduct) {
                renderProductCount =
                    <input
                        type='text'
                        readOnly
                        className={classes.Input}
                        value={filteredProduct.product.unit === 'ea'
                            ? filteredProduct.quantity
                            : filteredProduct.quantity.toFixed(1)} />;
                // onChange={(e) => this.inputHandler(e, this.props.product._id, filteredProduct.quantity)} 

                renderControls = (
                    <React.Fragment>
                        <Button variant="contained" color="primary"
                            onClick={() => this.props.onDecreaseQuantity(filteredProduct.product._id)}
                            className={classes.ControlsButton}
                            disabled={this.props.isShowCartDrawer &&
                                (filteredProduct.quantity === 1 || filteredProduct.quantity === 0.1)}>
                            <Remove />
                        </Button>
                        <Button variant="contained" color="primary"
                            onClick={() => this.props.onIncreaseQuantity(filteredProduct.product._id)}
                            className={classes.ControlsButton}>
                            <Add />
                        </Button>
                    </React.Fragment>
                )
            } 
        }

        return (
            <React.Fragment>
                <div className={this.props.isShowCartDrawer
                    ? [classes.ProductQuantityWrapper, classes.CartDrawerQuantityWrapper].join(' ')
                    : classes.ProductQuantityWrapper}>


                    <div className={classes.ProductQuantity}>
                        {renderProductCount}
                        <span className={classes.ProductUnit}>
                            {this.props.product.unit}
                        </span>
                    </div>
                    {renderControls}

                </div>
            </React.Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        cart: state.cart.cart
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddToCart: (productId) => dispatch(actions.addProductToCart(productId)),
        onIncreaseQuantity: (productId) => dispatch(actions.increaseProductQuantity(productId)),
        onDecreaseQuantity: (productId) => dispatch(actions.decreaseProductQuantity(productId)),
        onUpdateInputQuantity: (value, isIncrement, productId) => dispatch(actions.updateInputQuantity(value, isIncrement, productId))
    }
}

ProductControl.propTypes = {
    product: PropTypes.object
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductControl);



// const ProductControl = (props) => {

//     // console.log('[product control] ', props)
//     let renderControls = (
//         <Button variant="contained" color="primary"
//             onClick={() => props.onAddToCart(props.product._id)}>
//             Add
//         </Button>
//     )
//     if(props.cart) {
//         const targetProduct = props.cart.products.find(cartProduct => cartProduct.id === props.product._id)
//         if (targetProduct) {
//             renderControls = (
//                <React.Fragment>
//                    <Button variant="contained" color="primary"
//                         onClick={() => props.onDecreaseQuantity(targetProduct.id)}
//                         className={styles.ControlsButton}>
//                         -
//                     </Button>
//                     <Button variant="contained" color="primary"
//                         onClick={() => props.onIncreaseQuantity(targetProduct.id)}
//                         className={styles.ControlsButton}>
//                         +
//                     </Button>
//                 </React.Fragment>
//             )
//         } else {
//             renderControls = 
//                 <Button variant="contained" color="primary"
//                     onClick={() => props.onAddToCart(props.product)}>
//                     Add
//                 </Button>
//         }
//     }
//     return (
//         <React.Fragment>
//             { renderControls }
//         </React.Fragment>
//     )
// }