import React, { useState } from 'react'
import { Popover, Typography } from '@material-ui/core';

const MaterialPopover = props => {

    // const [anchorEl, setAnchorEl] = useState(null);

    // const handleClick = (event) => {
    //     console.log('popover ', event.currentTarget)
    //   setAnchorEl(event.currentTarget);
    // }
  
    // const handleClose = () => {
    //   setAnchorEl(null);
    // }

    // const open = Boolean(anchorEl)
    // const id = open ? 'simple-popover' : undefined;

    // console.log(props)
    return (
        <Popover
            id={props.id}
            open={props.open}
            anchorEl={props.anchorEl}
            onClose={props.onClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
        >
            <Typography style={{padding: '10px'}}>
                {props.popoverMessage}
            </Typography>
        </Popover>
    )
}

export default MaterialPopover;