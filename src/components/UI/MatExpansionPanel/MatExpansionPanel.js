import React, { Component } from 'react';
import {
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    List,
    ListItem,
    ListItemText
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'


class MatExpansionPanel extends Component {

    itemClickHandler = (event) => {
        console.log(event.target)
    }
    render() {
        
        return (
            <ExpansionPanel style={{ minHeight: '0' }}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} >
                    <Typography>
                        Deli & Chilled Foods
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails style={{ padding: '0' }}>
                    <div style={{
                        width: '100%'
                    }} >
                        <List component="a">
                            <ListItem button 
                                onClick={(e) => this.itemClickHandler(e)} >
                                <ListItemText primary="Bacon" 
                                    style={{padding: '0 15px'}} />
                            </ListItem>
                            <ListItem button>
                                <ListItemText primary="Butter & Dairy Spreads" 
                                    style={{padding: '0 15px'}}/>
                            </ListItem>
                        </List>
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}

export default MatExpansionPanel;