import React from 'react';
import { 
  Dialog, 
  DialogTitle, 
  DialogContent, 
  DialogContentText, 
  DialogActions,
  Button
} from '@material-ui/core';

class MaterialDialog extends React.Component {

  handleClose = (selectedValue) => {
    console.log('close dialog ', selectedValue);
    this.props.onClose(selectedValue);
  }

  renderAction() {
    if(this.props.actions === 'double') {
      return(
        <React.Fragment>
            <Button onClick={()=> this.handleClose(this.props.actionPositive)} color="primary" autoFocus>
              {this.props.actionPositive}
            </Button>
            <Button onClick={()=> this.handleClose(this.props.actionNegative)} color="primary">
              {this.props.actionNegative}
            </Button>
        </React.Fragment>
      )
    } else {
      return(
        <Button onClick={()=> this.handleClose(this.props.actionPositive)} color="primary" autoFocus>
            {this.props.actionPositive}
        </Button>
      )
    }
    
  }
  
  render() {
    // console.log('is dialog open? ', this.props.open);
    return(
        <Dialog onClose={this.handleClose} open={this.props.open}>
          <DialogTitle id="alert-dialog-title">{this.props.title}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
             {this.props.description}
            </DialogContentText>
          </DialogContent>

          <DialogActions>
            {this.renderAction()}
          </DialogActions>
        </Dialog>
    )
  }
}

export default MaterialDialog;