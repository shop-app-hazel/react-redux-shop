import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
    withStyles,
    Menu,
    MenuItem,
    IconButton,
    Button
} from '@material-ui/core';


const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})(props => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));


const MenuButton = (props) => {

    const ForwardNavLink = React.forwardRef((props, ref) => (
        <NavLink {...props} innerRef={ref} />
    ));

    const [anchorEl, setAnchorEl] = useState(null);

    const openMenuHandler = (e) => {
        setAnchorEl(e.currentTarget);
    }

    const closeMenuHandler = () => {
        setAnchorEl(null);
    }

    const onMenuItemClicked = (itemLink) => {
        props.history.push(itemLink)
        closeMenuHandler();
    }


    let renderButton = null;
    if (props.buttonTitle) {
        renderButton = (
            <Button
                style={{ fontWeight: 'bolder' }}
                color="inherit"
                onClick={openMenuHandler}>
                {props.buttonTitle}
            </Button>
        )
    }

    let renderButtonIcon = null;
    if (props.buttonIconType) {
        const Wrapper = props.buttonIconType;
        renderButtonIcon = (
            <IconButton color="inherit" onClick={openMenuHandler}>
                {<Wrapper />}
            </IconButton>
        )
    }

    return (
        <React.Fragment>
            {renderButtonIcon}

            {renderButton}

            {/* <Menu
                id={props.id}
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={closeMenuHandler}
            > */}
            <StyledMenu
                id={props.id}
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={closeMenuHandler}
            >
                {props.menuItems.map(item => {
                    return (
                        <MenuItem
                            component={ForwardNavLink}
                            to={item.link}
                            key={item.name}
                            onClick={() => onMenuItemClicked(item.link)}>
                            {item.name}
                        </MenuItem>
                    )
                })}
            </StyledMenu>
        </React.Fragment >
    )
}

MenuButton.propTypes = {
    id: PropTypes.string,
    buttonIconType: PropTypes.object,
    buttonTitle: PropTypes.string,
    menuItems: PropTypes.array
}

export default withRouter(MenuButton);