import React from 'react';
import classes from './MaterialButton.module.css';
import { Button } from '@material-ui/core';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';



const MaterialButton = props => {

    let renderIcon = null;
    if (props.btnIcon) {
        const Wrapper = props.btnIcon;
        renderIcon = (
            <React.Fragment>
                {<Wrapper />}
            </React.Fragment>
        )
    }

    return (
        <Button
            fullWidth={props.isFullWidth ? true : false}
            variant={props.btnVariant}
            color={props.btnColor}
            disabled={props.isDisabled}
            onClick={props.onButtonClicked}
            className={classes.Button}>
            {renderIcon}
            <span>{props.btnName}</span>

            {props.showIndicator
                ? props.isProcessing ? <LoadingIndicator /> : null
                : null
            }
        </Button>
    )
}

export default MaterialButton;