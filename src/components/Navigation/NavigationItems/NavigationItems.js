import React from 'react'
import NavigationItem from "./NavigationItem/NavigationItem";
import { List } from "@material-ui/core";
import {
    ShoppingCartOutlined,
    ReceiptOutlined,
    SupervisedUserCircleOutlined,
} from '@material-ui/icons';

const navItems = [
    { link: "/admin/products", icon: <ShoppingCartOutlined />, name: "Products" },
    { link: "/admin/orders", icon: <ReceiptOutlined />, name: "Orders" },
    { link: "/admin/users", icon: <SupervisedUserCircleOutlined />, name: "Users" }
]
const NavigationItems = (props) => {
    return (
        <List>
            {navItems.map((item, index) => (
                <NavigationItem
                    key={index} 
                    link={item.link} 
                    icon={item.icon}
                    navClicked={props.onNavClicked}
                    name={item.name} />
            ))}
        </List>
    )
}

export default NavigationItems;