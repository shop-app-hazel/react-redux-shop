import React from 'react';
import { Link } from 'react-router-dom';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

const NavigationItem = (props) => {
    // console.log('[NavigationItem.js] ', props.icon)
    const ForwardLink = React.forwardRef((props, ref) => (
        <Link {...props} innerRef={ref} />
    ));

    const Wrapper = props.icon.type;

    return (
        <ListItem
            key={props.name}
            button
            to={props.link}
            onClick={props.navClicked}
            component={ForwardLink}>

            {props.icon && <ListItemIcon>{<Wrapper />}</ListItemIcon>}
            <ListItemText primary={props.name} />
        </ListItem>
    )
}

export default NavigationItem;