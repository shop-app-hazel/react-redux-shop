import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import classes from './Navbar.module.css';
import Logo from '../../Logo/Logo';
import Login from '../../../containers/Auth/Login';
import MenuButton from '../../UI/MenuButton/MenuButton';
import MaterialPopover from '../../UI/MaterialPopover/MaterialPopover';
import { AccountCircle, ShoppingCartOutlined, Search, CloseOutlined } from '@material-ui/icons'
import {
    AppBar,
    Toolbar,
    IconButton,
    Badge,
    Button,
    Dialog,
    InputBase,
    Paper
} from '@material-ui/core';

const accountItems = [
    { name: 'My Account', link: '/account' },
    { name: 'My Order', link: '/orders' },
    { name: 'Logout', link: '/logout' }
];

class Navbar extends React.Component {
    state = {
        isDialogOpen: false,
        searchInput: "",
        anchorEl: null
    }

    componentDidMount() {
        this.props.onInitCart();
    }

    loginHandler = () => {
        this.setState({ isDialogOpen: true })
    }

    closeDialogHandler = () => {
        this.setState({ isDialogOpen: false })
    }


    searchInputHandler = (e) => {
        this.setState({ searchInput: e.target.value })
    }

    deleteSearchHandler = () => {
        this.setState({ searchInput: "" })
    }

    searchHandler = async () => {
        await this.props.onSearchItem(this.state.searchInput)
        this.props.history.push({
            pathname: '/products',
            search: `?search=${this.state.searchInput}`
        })
    }

    shoppingCartClickHandler = (event) => {
        if(this.props.cart) {
            if(this.props.cart.products.length > 0) {
                this.props.onCartClicked()
            } else {
                this.popoverClickHandler(event)
            }
        } else {
            this.popoverClickHandler(event)
        }
    }

    popoverClickHandler = (event) => {
        // console.log('popover ', event.currentTarget)
        this.setState({anchorEl: event.currentTarget});
    }

    popoverCloseHandler = () => {
        this.setState({anchorEl: null});
    }



    render() {
        const open = Boolean(this.state.anchorEl)
        const id = open ? 'simple-popover' : undefined;

        // console.log('[Navbar.js] ', this.props)
        return (
            <React.Fragment>
                <AppBar position="static">
                    <Toolbar>
                        <Logo />
                        <div className={classes.Spacer}></div>
                        <Paper className={classes.SearchInputWrapper}>
                            <InputBase
                                className={classes.Input}
                                placeholder="Search"
                                value={this.state.searchInput}
                                onChange={(e) => { this.searchInputHandler(e) }}
                            />
                            {this.state.searchInput.trim() !== ""
                                ? <span className={classes.CloseIcon} >
                                    <IconButton onClick={this.deleteSearchHandler} >
                                        <CloseOutlined />
                                    </IconButton>
                                </span>
                                : null}

                            <hr className={classes.Divider} />

                            <IconButton
                                className={classes.iconButton}
                                onClick={this.searchHandler}>
                                <Search />
                            </IconButton>
                        </Paper>
                        <div className={classes.Spacer}></div>

                        <IconButton color="default" onClick={e => this.shoppingCartClickHandler(e)} >
                            <Badge
                                className={classes.margin}
                                badgeContent={this.props.cart ? this.props.cart.totalQuantity : null}
                                color="secondary">
                                <ShoppingCartOutlined className={classes.ShoppingCartIcon} />
                            </Badge>
                        </IconButton>

                        <MaterialPopover 
                            id={id}
                            open={open}
                            anchorEl={this.state.anchorEl}
                            onClose={this.popoverCloseHandler}
                            popoverMessage="Your trolley is empty" />
        
                        {this.props.isAuthenticated
                            ? <MenuButton
                                id='accountMenu'
                                buttonIconType={AccountCircle}
                                menuItems={accountItems} />
                            : <Button color="primary" onClick={this.loginHandler}>
                                <span style={{ color: 'white' }}>Login</span>
                            </Button>
                        }
                    </Toolbar>
                </AppBar>

                <Dialog open={this.state.isDialogOpen} onClose={this.closeDialogHandler}>
                    <Login onAction={this.closeDialogHandler} />
                </Dialog>
            </React.Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart.cart,
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onInitCart: () => dispatch(actions.initCart()),
        onSearchItem: (input) => dispatch(actions.searchProducts(input))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar));