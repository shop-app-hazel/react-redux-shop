import React, { Component } from 'react';
import classes from './Layout.module.css';
import Navbar from '../../components/Navigation/Navbar/Navbar';
import CartDrawer from '../CartDrawer/CartDrawer';
import Footer from '../../components/Footer/Footer';

class Layout extends Component {
    state = {
        isCartClicked: false
    }

    openMenuHandler = () => {
        this.setState({isMenuClicked: true})
    }
    
    openCartHandler = () => {
        this.setState({isCartClicked: true})
    }

    closeCartHandler = () => {
        this.setState({isCartClicked: false})
    }
    render() {
        return (
            <React.Fragment>
                <Navbar
                    onMenuClicked={this.openMenuHandler}
                    onCartClicked={this.openCartHandler} />
                <CartDrawer 
                    onDrawerClosed={this.closeCartHandler} 
                    cartClicked={this.state.isCartClicked} />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
                <Footer />
            </React.Fragment>
        )
    }
}

export default Layout;