import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../../App.css'
import { Grid } from '@material-ui/core'
import CheckoutPaymentForm from '../../components/Checkout/CheckoutPayment/CheckoutPaymentForm';
import CheckoutSummary from '../../components/Checkout/CheckoutSummary/CheckoutSummary';


class CheckoutPayment extends Component {
    render() {
        return (
            <div className="Container">
                <div className="HeaderWrapper">
                    <span className="HeaderTitle">
                        Payment Information
                    </span>
                </div>

                <Grid container spacing={2}>
                    <Grid item md={7} xs={12}>
                        <CheckoutPaymentForm />
                    </Grid>

                    <Grid item md={1}></Grid>

                    <Grid item md={4} xs={12}>
                        {this.props.cart
                            ? <CheckoutSummary
                                cart={this.props.cart}
                                isValid={this.props.isPaymentFormValid} />
                            : null}
                    </Grid>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart.cart,
        isPaymentFormValid: state.checkout.isPaymentFormValid
    }
}

export default connect(mapStateToProps)(CheckoutPayment);