import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import '../../App.css';
import MaterialButton from '../../components/UI/MaterialButton/MaterialButton';
import CheckoutDetails from '../../components/Checkout/CheckoutDetails/CheckoutDetails';
import CheckoutSummary from '../../components/Checkout/CheckoutSummary/CheckoutSummary';

import { AlarmAddRounded } from '@material-ui/icons';
import { Grid } from '@material-ui/core';

const Checkout = props => {

    const selectTimeslotHandler = () => {
        alert('Function not complete')
    }

    const cancelProductHandler = (product) => {
        // console.log('item cancelled ', product)
        props.onCancelProduct(product._id)
    }

    return (
        <div className="Container">
            <div className="HeaderWrapper">
                <span className="HeaderTitle">
                    Order Details
                </span>

                <MaterialButton
                    btnVariant="contained"
                    btnColor="secondary"
                    btnIcon={AlarmAddRounded}
                    btnName="select timeslot"
                    onButtonClicked={selectTimeslotHandler} />
            </div>


            <Grid container spacing={2}>
                <Grid item md={8} xs={12}>
                    {props.cart
                        ? props.cart.totalQuantity > 0
                            ? <CheckoutDetails
                                cart={props.cart}
                                onCancelProduct={cancelProductHandler} />
                            : <p>You have no item selected.</p>
                        : <p>You have no item selected.</p>
                    }
                </Grid>

                <Grid item md={4} xs={12}>
                    {props.cart
                        ? <CheckoutSummary
                            cart={props.cart}
                            isValid={props.cart.totalQuantity > 0} />
                        : null}
                </Grid>
            </Grid>

        </div>
    )
}

const mapStateToProps = state => {
    return { cart: state.cart.cart }
}

const mapDispatchToProps = dispatch => {
    return {
        onCancelProduct: (productId) => dispatch(actions.cancelProduct(productId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);