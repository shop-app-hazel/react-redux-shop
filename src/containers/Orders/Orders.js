import React from 'react';
import '../../App.css'
import OrderList from '../../components/Orders/OrderList';

const Orders = props => {
    return (
        <div className="Container">
            <div className="HeaderWrapper">
                <span className="HeaderTitle">
                    Order History
                </span>
            </div>

            <OrderList />
        </div>
    )
}

export default Orders;