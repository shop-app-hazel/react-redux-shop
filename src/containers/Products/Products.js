import React, { Component } from 'react';
import classes from './Product.module.css';
import { capitalize } from '../../shared/utility';
import ProductList from '../../components/Products/ProductList';
import Breadcrumb from '../Breadcrumb/Breadcrumb';
import { Grid } from '@material-ui/core';

class Products extends Component {
    render() {
        const bcNavLinks = [
            { name: 'Groceries', linkTo: '/' }
        ]

        const filterProduct =
            this.props.location.pathname.substr(10)
                ? capitalize(this.props.location.pathname.substr(10))
                : ''

        return (
            <div className={classes.ProductContainer}>
                <Breadcrumb
                    prevLinks={bcNavLinks}
                    currentName={filterProduct} />

                <Grid container>
                    <Grid item xs={12}>
                        <ProductList
                            filter={filterProduct} />
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default Products;