import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from '../../store/actions/index'
import classes from './Login.module.css'
import MaterialButton from '../../components/UI/MaterialButton/MaterialButton'
import Input from '../../components/UI/Input/Input'
import { checkValidation } from '../../shared/utility';
import {
    Grid,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField
} from '@material-ui/core'

class Login extends Component {
    state = {
        error: null,
        isLogin: true,
        isFormValid: false,
        resetPasswordEmail: "",
        loginForm: {
            email: {
                label: 'Email Address',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true,
                    isEmail: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            password: {
                label: 'Password',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'password',
                    placeholder: ''
                },
                validationRules: {
                    required: true,
                    minLength: 6
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
        }
    }

    inputChangedHandler = (e, formId) => {
        const updatedForm = {
            ...this.state.loginForm,
            [formId]: {
                ...this.state.loginForm[formId],
                value: e.target.value,
                validation: checkValidation(e.target.value, this.state.loginForm[formId].validationRules),
                touched: true
            }
        }

        let isFormValid = true;
        for (let element in updatedForm) {
            isFormValid = updatedForm[element].validation.isValid && isFormValid;
        }
        this.setState({ loginForm: updatedForm, isFormValid })
    }

    submitLoginFormHandler = async (e) => {
        e.preventDefault();

        const formData = {};
        for (let formElement in this.state.loginForm) {
            formData[formElement] = this.state.loginForm[formElement].value;
        }
        // console.log('[Login.js] ', formData);

        await this.props.onLogin(formData);
        if (!this.props.error) {
            // console.log("No Error")
            this.setState({ error: null })
            this.props.onAction();
        } else {
            this.setState({ error: this.props.error })
        }
    }

    resetPasswordHandler = (e) => {
        e.preventDefault();

        //TODO: submit forgotten password email
    }

    actionHandler = (actionType) => {
        switch (actionType) {
            case 'signup':
                this.props.onAction();
                this.props.history.push('/signup')
                break;

            case 'forgotPwd':
                this.setState({ isLogin: false })
                break;

            case 'backToLogin':
                this.setState({ isLogin: true })
                break;

            default: break;
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.loginForm) {
            formElementsArray.push({
                id: key,
                config: this.state.loginForm[key]
            });
        }

        let dialogContent = (
            <React.Fragment>
                <DialogTitle className={classes.DialogTitle}>
                    Login to your Account
                </DialogTitle>
                
                {this.state.error
                    ? <p className={classes.ErrorMessage}> {this.state.error} </p>
                    : null
                }

                <DialogContent className={classes.Content} >
                    <p>New customer?&nbsp;
                        <span className={classes.ActionLink}
                            onClick={() => this.actionHandler('signup')}>
                            Please register
                        </span>
                    </p>
                    <form>
                        {formElementsArray.map(formElement => (
                            <Grid item xs={12} key={formElement.id}>
                                <Input
                                    id={formElement.id}
                                    label={formElement.config.label}
                                    value={formElement.config.value}
                                    helperText={formElement.config.helperText}
                                    elementType={formElement.config.elementType}
                                    elementConfig={formElement.config.elementConfig}
                                    isValid={formElement.config.validation.isValid}
                                    isTouched={formElement.config.touched}
                                    errorMessage={formElement.config.validation.errorMessage}
                                    onInputChanged={(e) => this.inputChangedHandler(e, formElement.id)}
                                />
                            </Grid>
                        ))}
                    </form>
                </DialogContent>

                <DialogActions className={classes.ActionContainer}>
                    <MaterialButton
                        btnVariant="contained"
                        btnColor="primary"
                        btnName="Login"
                        isDisabled={!this.state.isFormValid}
                        onButtonClicked={this.submitLoginFormHandler} >
                    </MaterialButton>

                    <span className={classes.ActionLink}
                        onClick={() => this.actionHandler('forgotPwd')}>
                        Forgotten Password
                    </span>
                </DialogActions>
            </React.Fragment>
        )

        if (!this.state.isLogin) {
            dialogContent = (
                <React.Fragment>
                    <DialogTitle className={classes.DialogTitle}>
                        Forgotten Password
                    </DialogTitle>
                    <DialogContent className={classes.Content} >
                        <p>Enter the email address you use to login and
                            we will send you a link to reset your password
                        </p>

                        <TextField
                            autoFocus
                            margin="dense"
                            id="email"
                            label="Email Address"
                            type="email"
                            fullWidth
                            value={this.state.resetPasswordEmail}
                            onChange={(e) => this.setState({ resetPasswordEmail: e.target.value })}
                        />
                        <span className={classes.ActionLink} style={{ float: 'right' }}
                            onClick={() => this.actionHandler('backToLogin')}>
                            Back to Login
                        </span>
                    </DialogContent>

                    <DialogActions className={classes.ActionContainer}>
                        <MaterialButton
                            btnVariant="contained"
                            btnColor="primary"
                            btnName="Reset"
                            isDisabled={this.state.resetPasswordEmail === ""}
                            onButtonClicked={this.resetPasswordHandler} >
                        </MaterialButton>
                    </DialogActions>
                </React.Fragment>
            )
        }


        return (
            <React.Fragment>
                {dialogContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        error: state.auth.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (formData) => dispatch(actions.auth(formData))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));