import React from 'react';
import { Drawer } from '@material-ui/core'
import CartDrawerContent from '../../components/CartDrawerContent/CartDrawerContent';

const CartDrawer = (props) => {
   
    const closeDrawerHandler = () => event => {
        // if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
        //     return;
        // }
        props.onDrawerClosed();
    };

    let renderDrawer = null;
    if (props.cartClicked) {
        renderDrawer = 
            <Drawer anchor="right" 
                open={props.cartClicked}
                onClose={closeDrawerHandler()} >
                <CartDrawerContent 
                    onCloseDrawer={closeDrawerHandler()}/>   
            </Drawer>;
    }
    return (
        <React.Fragment>
            {renderDrawer}
        </React.Fragment>
    )
}
export default CartDrawer;