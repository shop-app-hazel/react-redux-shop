import React from 'react'
import { NavLink } from 'react-router-dom'
import classes from './Breadcrumb.module.css'
import { Breadcrumbs, Typography } from '@material-ui/core';
import { NavigateNext } from '@material-ui/icons';

const Breadcrumb = props => {
    return (
        <div className={classes.BreadcrumbsContainer}>
            <Breadcrumbs separator={<NavigateNext fontSize="small" />}>
                {props.prevLinks.map(link => (
                    <NavLink 
                        key={link.name}
                        color="inherit" to={link.linkTo}
                        activeClassName={classes.BreadcrumbLink} >
                        {link.name}
                    </NavLink>
                ))}
                <Typography color="textPrimary">
                    {props.currentName}
                </Typography>
            </Breadcrumbs>
        </div>
    )
}

export default Breadcrumb;