import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import '../../App.css';
import classes from './Categories.module.css';
import { Grid } from '@material-ui/core';
import Category from '../../components/Category/Category';
import Spinner from '../../components/UI/Spinner/Spinner';


class Categories extends Component {
    componentDidMount() {
        this.props.onFetchCategories();
    }

    // shouldComponentUpdate(nextProps, nextState) {
    // if(nextProps.categories !== this.props.categories) {
    //     return true;
    // } else {
    //     return false;
    // }
    // }

    selectCategoryHandler = (category) => {
        const categoryName = (category.name).toLowerCase();
        this.props.history.push(`/category/${categoryName}`);
    }

    render() {
        // console.log('[Categories] rendering...')
        let renderCategories = <Spinner />;
        if (!this.props.loading) {
            renderCategories =
                <div className="Container">
                    <div className="HeaderWrapper" style={{flexDirection: 'column'}}>
                        <h1 className={classes.Title}>Shop Categories</h1>
                        <Grid container spacing={3}>
                        {this.props.categories.map(category => (
                            <Category
                                key={category._id}
                                onSelectCategory={this.selectCategoryHandler}
                                category={category} />
                        ))}
                        </Grid>
                    </div>
                </div>
        }

        return (
            <React.Fragment>
                {renderCategories}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        categories: state.categories.categories,
        loading: state.categories.loading
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchCategories: () => dispatch(actions.fetchCategories())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories);